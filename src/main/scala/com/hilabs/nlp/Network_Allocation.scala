package com.hilabs.nlp

import com.hilabs.config.HilabsConfig
import com.hilabs.dao.util.DatabaseConnector
import org.apache.spark.sql.SparkSession

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Network_Allocation {

	def allocateNetworks(connection_id: Int,
	                     usecase_id: Int): Unit ={

		val spark = SparkSession.builder.master("yarn").
			appName("Indexing_NLP_Explore_By_Source").
			getOrCreate

		import spark.implicits._

		val conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql")
		DatabaseConnector.connectToDatabase(conf)
		val connection = DatabaseConnector.connection
		val statement = connection.createStatement()

		val query_1 = s"""select target_feature from labels_library where connection_id=$connection_id and label_set_id=$usecase_id"""
		val result_q1 = statement.executeQuery(query_1)
		result_q1.next()
		val column_names = result_q1.getString("target_feature").split(",")
		println(s"Target Feature(s): ${column_names.mkString("Array(", ", ", ")")}")

		val query_2 = s"""select associatedConnections from labels_library where connection_id=$connection_id and label_set_id=$usecase_id"""
		val result_q2 = statement.executeQuery(query_2)
		result_q2.next()
		val feature_conns = result_q2.getString("associatedConnections").split(",")
		val num_features = feature_conns.size
		println(s"Associated Connection(s): ${feature_conns.mkString("Array(", ", ", ")")}")

		var insert_query = ""
		var delete_query = ""

		for (i <- 0 until num_features){
			val feature_name = column_names(i)
			val feature_conn_id = feature_conns(i)
			println(s"Reading Schema from connection: $feature_conn_id")
			val df = spark.read.load(s"""/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/connection-$feature_conn_id""")
			val feature_values = df.select(feature_name).as[String].collect.toList.mkString(",")
			println(s"Feature_Values: $feature_values")
			val current_timestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now())
			delete_query = s"""delete from nlp_product_features where base_connection_id=$connection_id and usecase_id=$usecase_id and feature_solr_name="$feature_name""""
			println(s"Executing Query: $delete_query")
			val statement = connection.createStatement()
			statement.executeUpdate(delete_query)
			insert_query = s"""insert into nlp_product_features (base_connection_id, usecase_id, feature_solr_name, feature_conn_id, feature_entities, isActive, modification_date) values ($connection_id, $usecase_id, '$feature_name', $feature_conn_id, "$feature_values", 1, '$current_timestamp')"""
			println(s"Executing Query: $insert_query")
			statement.executeUpdate(insert_query)
		}

	}
}
