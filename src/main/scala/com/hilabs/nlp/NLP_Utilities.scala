package com.hilabs.nlp

import com.hilabs.config.HilabsConfig
import com.hilabs.dao.util.DatabaseConnector
import com.hilabs.solr.Spark_solr
import com.hilabs.utilities.{DBUtility, FSUtils}
import org.apache.spark.sql.SparkSession

class NLP_Utilities {

	def index_explore_by_source(connection_id: Int,
	                            useCase_ID: Int,
	                            SEED_PATH: String): Unit = {

		println(s"Starting Indexing of Explore by Source for Connection: $connection_id and UseCase: $useCase_ID")

		val conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql")
		val _ = DatabaseConnector.connectToDatabase(conf)
		val spark = SparkSession.builder.master("yarn").
			appName("Indexing_NLP_Explore_By_Source").
			getOrCreate
		val sc = spark.sparkContext

		val basePath = SEED_PATH + "connection-" + connection_id + "/usecase_" + useCase_ID

		if (!FSUtils.checkIfPathExistsInHadoop(basePath, sc)) {
			FSUtils.makeDirectoryInHDFS(SEED_PATH, basePath, "", sc)
		}

		val source_cols = Array("DocumentName", "Feature_Type", "Type", "Findings")
		val source_maps = DBUtility.genMapsFromValuesets(DatabaseConnector.connection, source_cols)
		val source_name_to_solr_col_map = source_maps._1
		val source_solr_col_to_name_map = source_name_to_solr_col_map.map(_.swap)
		val source_solr_col_to_datatype_map = source_maps._2

		var labels_df = spark.read.parquet(basePath + "/explore_by_source")
		val selectCols = labels_df.columns.map(c => labels_df(c).as(source_name_to_solr_col_map(c)))
		labels_df = labels_df.select(selectCols: _*)

		labels_df.write.mode("overwrite").save(basePath + "/explore_by_source_df")

		println(s"Inserting Explore by Source into connectionlist table.")

		val labels_connection_id = DBUtility.insertLabelsIntoConnectionList(DatabaseConnector.connection, basePath, useCase_ID, "Source", "explore_by_source_df", source_solr_col_to_name_map)

		val project_id = DBUtility.getProjectID(DatabaseConnector.connection, connection_id)

		println(s"The Project ID for base connection is: $project_id")

		val query = s"""insert into connection_project_details (connection_id, project_id) values ($labels_connection_id, $project_id)"""
		val statement = DatabaseConnector.connection.createStatement()

		statement.executeUpdate(query)

		DBUtility.insertIntoTempValuesets(DatabaseConnector.connection, labels_df, labels_connection_id, source_solr_col_to_name_map, source_solr_col_to_datatype_map)

		labels_df.write.save(SEED_PATH + "connection-" + labels_connection_id)

		Spark_solr.justIndexParquetToSolrNonDelete(labels_df, "connection-" + labels_connection_id)

		DBUtility.updateLabelsLibraryDetails(DatabaseConnector.connection, connection_id, useCase_ID, "Source", labels_connection_id)

		println(s"The Connection ID for Explore by Source is: $labels_connection_id")

		DatabaseConnector.connection.close()
	}

	def index_explore_by_source_drilled_down(connection_id: Int,
	                                         useCase_ID: Int,
	                                         SEED_PATH: String): Unit = {

		println(s"Starting Indexing of Explore by Source Drilled Down for Connection: $connection_id and UseCase: $useCase_ID")

		val conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql")
		val _ = DatabaseConnector.connectToDatabase(conf)
		val spark = SparkSession.builder.master("yarn").
			appName("Indexing_NLP_Explore_By_Source_Drilled_Down").
			getOrCreate
		val sc = spark.sparkContext

		val basePath = SEED_PATH + "connection-" + connection_id + "/usecase_" + useCase_ID

		if (!FSUtils.checkIfPathExistsInHadoop(basePath, sc)) {
			FSUtils.makeDirectoryInHDFS(SEED_PATH, basePath, "", sc)
		}

		val source_cols = Array("DocumentName", "Provider_Federal_Tax_Identifier", "Feature_Type", "Network_Set", "Target_Network_Set", "Additional_Network_Name", "Missing_Network_Name", "Findings", "Usecase_ID")
		val source_maps = DBUtility.genMapsFromValuesets(DatabaseConnector.connection, source_cols)
		val source_name_to_solr_col_map = source_maps._1
		val source_solr_col_to_name_map = source_name_to_solr_col_map.map(_.swap)
		val source_solr_col_to_datatype_map = source_maps._2

		var labels_df = spark.read.parquet(basePath + "/explore_by_source_drilled_down")
		val selectCols = labels_df.columns.map(c => labels_df(c).as(source_name_to_solr_col_map(c)))
		labels_df = labels_df.select(selectCols: _*)

		labels_df.write.mode("overwrite").save(basePath + "/explore_by_source_drilled_down_df")

		println(s"Inserting Explore by Source Drilled Down into connectionlist table.")

		val labels_connection_id = DBUtility.insertLabelsIntoConnectionList(DatabaseConnector.connection, basePath, useCase_ID, "Source_Drill_Down", "explore_by_source_drilled_down_df", source_solr_col_to_name_map)

		val project_id = DBUtility.getProjectID(DatabaseConnector.connection, connection_id)

		println(s"The Project ID for base connection is: $project_id")

		val query = s"""insert into connection_project_details (connection_id, project_id) values ($labels_connection_id, $project_id)"""
		val statement = DatabaseConnector.connection.createStatement()

		statement.executeUpdate(query)

		DBUtility.insertIntoTempValuesets(DatabaseConnector.connection, labels_df, labels_connection_id, source_solr_col_to_name_map, source_solr_col_to_datatype_map)

		labels_df.write.save(SEED_PATH + "connection-" + labels_connection_id)

		Spark_solr.justIndexParquetToSolrNonDelete(labels_df, "connection-" + labels_connection_id)

		DBUtility.updateLabelsLibraryDetails(DatabaseConnector.connection, connection_id, useCase_ID, "Drill", labels_connection_id)

		println(s"The Connection ID for Explore by Source Drilled Down is: $labels_connection_id")

		DatabaseConnector.connection.close()
	}

	def index_explore_by_feature(connection_id: Int,
	                             useCase_ID: Int,
	                             SEED_PATH: String): Unit = {

		println(s"Starting Indexing of Explore by Feature for Connection: $connection_id and UseCase: $useCase_ID")

		val conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql")
		val _ = DatabaseConnector.connectToDatabase(conf)
		val spark = SparkSession.builder.master("yarn").
			appName("Indexing_NLP_Explore_By_Feature").
			getOrCreate
		val sc = spark.sparkContext

		val basePath = SEED_PATH + "connection-" + connection_id + "/usecase_" + useCase_ID

		if (!FSUtils.checkIfPathExistsInHadoop(basePath, sc)) {
			FSUtils.makeDirectoryInHDFS(SEED_PATH, basePath, "", sc)
		}

		val source_cols = Array("Anomaly", "Feature_Type", "Type", "Findings")
		val source_maps = DBUtility.genMapsFromValuesets(DatabaseConnector.connection, source_cols)
		val source_name_to_solr_col_map = source_maps._1
		val source_solr_col_to_name_map = source_name_to_solr_col_map.map(_.swap)
		val source_solr_col_to_datatype_map = source_maps._2

		var labels_df = spark.read.parquet(basePath + "/explore_by_feature")
		val selectCols = labels_df.columns.map(c => labels_df(c).as(source_name_to_solr_col_map(c)))
		labels_df = labels_df.select(selectCols: _*)

		labels_df.write.mode("overwrite").save(basePath + "/explore_by_feature_df")

		println(s"Inserting Explore by Feature into connectionlist table.")

		val labels_connection_id = DBUtility.insertLabelsIntoConnectionList(DatabaseConnector.connection, basePath, useCase_ID, "Feature", "explore_by_feature_df", source_solr_col_to_name_map)

		val project_id = DBUtility.getProjectID(DatabaseConnector.connection, connection_id)

		println(s"The Project ID for base connection is: $project_id")

		val query = s"""insert into connection_project_details (connection_id, project_id) values ($labels_connection_id, $project_id)"""
		val statement = DatabaseConnector.connection.createStatement()

		statement.executeUpdate(query)

		DBUtility.insertIntoTempValuesets(DatabaseConnector.connection, labels_df, labels_connection_id, source_solr_col_to_name_map, source_solr_col_to_datatype_map)

		labels_df.write.save(SEED_PATH + "connection-" + labels_connection_id)

		Spark_solr.justIndexParquetToSolrNonDelete(labels_df, "connection-" + labels_connection_id)

		DBUtility.updateLabelsLibraryDetails(DatabaseConnector.connection, connection_id, useCase_ID, "Feature", labels_connection_id)

		println(s"The Connection ID for Explore by Feature is: $labels_connection_id")

		DatabaseConnector.connection.close()

	}

}
