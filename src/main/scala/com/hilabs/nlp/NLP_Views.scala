package com.hilabs.nlp

import com.hilabs.config.HilabsConfig
import com.hilabs.dao.util.DatabaseConnector
import com.hilabs.utilities.DBUtility
import com.hilabs.utilities.FSUtils.mkNull2Empty
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

object NLP_Views {

	def generate_views(connection_id: Int,
	                   usecase_id: Int,
	                   SEED_PATH: String): Unit ={

		val spark = SparkSession.builder.master("yarn").
			appName("Indexing_NLP_Explore_By_Source").
			getOrCreate
		val _ = spark.sparkContext

		val conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql")
		val con = DatabaseConnector.connectToDatabase(conf)

		println(s"Connection has been established as: $con")

		val connection = DatabaseConnector.connection
		val connection_path = f"${SEED_PATH}connection-$connection_id/"
		val usecase_path = f"${connection_path}usecase_$usecase_id/"
		val predictions_path = f"${usecase_path}predictions/"

		val concat_target_string = "Target_"
		val source_document_id = "DocumentName"
		var anomaly_col = "Anomaly"
		var anomaly_type_col = "Anomaly_Type"

		val features_tuple = DBUtility.getSourceLabelsKeyTargetFeatures(connection, connection_id, usecase_id)
		var source_keys = DBUtility.genColumnNamesFromValuesets(DatabaseConnector.connection, features_tuple._2)
		var source_values =DBUtility.genColumnNamesFromValuesets(DatabaseConnector.connection, features_tuple._3)
		val source_labels = DBUtility.genColumnNamesFromValuesets(DatabaseConnector.connection, features_tuple._1)
		val source_cols = source_labels ++ Array(source_document_id, anomaly_col, anomaly_type_col)
		val source_maps = DBUtility.genMapsFromValuesets(DatabaseConnector.connection, source_cols)
		val source_name_to_solr_col_map = source_maps._1
		anomaly_col = source_name_to_solr_col_map(anomaly_col)
		anomaly_type_col = source_name_to_solr_col_map(anomaly_type_col)

		val feature_extracted = source_values(0)
		val add_col_name = "Additional_" + feature_extracted
		val missing_col_name = "Missing_" + feature_extracted

		val target_conn_id = DBUtility.getTargetConnectionId(connection, connection_id, usecase_id)
		val project_id = DBUtility.getProjectID(DatabaseConnector.connection, target_conn_id)

		println(s"Project ID is: $project_id")

		var target_keys = source_keys.map(x=>concat_target_string+x)
		var target_values = source_values.map(x=>concat_target_string+x)

		val temp_labelDfs = source_labels.foldLeft(List[DataFrame]())((dfs, label) => {
			println(predictions_path + label + ".csv")
			var df = spark.read.format("csv").option("header", "true").load(predictions_path + label + ".csv")
			df = df.withColumnRenamed("entity", label)
			df = DBUtility.mkNULL(df)
			dfs :+ df
		})

		val labelDfs = List(temp_labelDfs.head.distinct, temp_labelDfs(1).dropDuplicates("Provider_Federal_Tax_Identifier"))

		var label_structured_data = labelDfs.reduce((df1, df2) => {
			df1.join(df2, Seq(source_document_id), "full_outer")
		})

		val filter_condition = (source_keys ++ source_values).map(label_structured_data(_).isNotNull).reduce(_ && _)
		label_structured_data = label_structured_data.filter(filter_condition)

		val rolledCols = label_structured_data.columns.filter(c => !source_keys.contains(c))
		label_structured_data = label_structured_data.groupBy(source_keys.head, source_keys.tail: _*).agg(rolledCols.map(x => concat_ws("|", sort_array(collect_set(col(x)))) as x).head, rolledCols.map(x => concat_ws("|", sort_array(collect_set(col(x)))) as x).tail: _*)

		val selectCols = label_structured_data.columns.map(c => label_structured_data(c).as(source_name_to_solr_col_map(c)))
		label_structured_data = label_structured_data.select(selectCols: _*)

		source_keys = source_keys.map(source_name_to_solr_col_map(_))
		source_values = source_values.map(source_name_to_solr_col_map(_))

		var target_df = DBUtility.mkNULL(spark.read.load(SEED_PATH + "connection-" + target_conn_id))

		val target_raw_maps = DBUtility.genMapsFromTempValuesets(DatabaseConnector.connection, target_conn_id, target_df.columns)
		val target_maps = (target_raw_maps._1.map(x => (concat_target_string + x._1) -> (concat_target_string + x._2)), target_raw_maps._2.map(x => (concat_target_string + x._1) -> x._2))
		val target_solr_col_to_name_map = target_maps._1
		val target_name_to_solr_col_map = target_solr_col_to_name_map.map(_.swap)
		target_keys = target_keys.map(target_name_to_solr_col_map(_))
		target_values = target_values.map(target_name_to_solr_col_map(_))
		target_df = target_df.columns.foldLeft(target_df)((df,col)=>df.withColumnRenamed(col,concat_target_string+col))

		val source_target_key_tuples = source_keys.zip(target_keys)
		val join_condition = source_target_key_tuples.map(x => {
			val source_key = x._1
			val target_key = x._2
			label_structured_data(source_key) === target_df(target_key)
		}).reduce(_ && _)
		var joined_df = label_structured_data.join(target_df, join_condition, "inner")

		val corr_col = "Target_" +  source_name_to_solr_col_map(feature_extracted)

		joined_df = joined_df.withColumn("S", trim(col(corr_col), "|")).drop(corr_col).withColumnRenamed("S", corr_col)
		joined_df = joined_df.na.fill("", Array(corr_col))
		var temp_df = joined_df.withColumn("S", split(col(corr_col), "\\|")).withColumn("Exploded_S", explode(col("S"))).drop("S")
		temp_df = temp_df.groupBy(source_name_to_solr_col_map("DocumentName"), source_name_to_solr_col_map("Provider_Federal_Tax_Identifier")).agg(concat_ws("|", sort_array(collect_set(col("Exploded_S")))) as "A")
		temp_df = temp_df.withColumn("A", trim(col("A"), "|"))
		joined_df = joined_df.join(temp_df, Seq(source_name_to_solr_col_map("DocumentName"), source_name_to_solr_col_map("Provider_Federal_Tax_Identifier")), "inner").drop(corr_col).withColumnRenamed("A", corr_col)

		val setSubtract = udf((x: String, y: String) => {
			val x_list =
				if (x != null) x.split('|')
				else Array[String]()
			val y_list =
				if (y != null) y.split('|')
				else Array[String]()
			val x_set = x_list.toSet
			val y_set = y_list.toSet
			val diff_set = x_set diff y_set
			val diff_set_piped = diff_set.mkString("|")
			diff_set_piped
		})

		val source_target_value_tuples = source_values.zip(target_values)
		var final_anomaly_df = source_target_value_tuples.foldLeft(spark.emptyDataFrame)((final_anomaly_df, x) => {
			val source_val = x._1
			val target_val = x._2
			val missing_anomaly_df = joined_df.
				withColumn(anomaly_col, setSubtract(col(source_val), col(target_val))).
				withColumn(anomaly_col, explode(split(col(anomaly_col), "\\|"))).
				withColumn(anomaly_type_col, lit(missing_col_name)).
				filter(col(anomaly_col) =!= "")
			val additional_anomaly_df = joined_df.
				withColumn(anomaly_col, setSubtract(col(target_val), col(source_val))).
				withColumn(anomaly_col, explode(split(col(anomaly_col), "\\|"))).
				withColumn(anomaly_type_col, lit(add_col_name)).
				filter(col(anomaly_col) =!= "")
			if (final_anomaly_df.head(1).isEmpty) {
				missing_anomaly_df.union(additional_anomaly_df)
			} else {
				final_anomaly_df.union(missing_anomaly_df).union(additional_anomaly_df)
			}
		})

		val name_to_solr_col_map = source_name_to_solr_col_map ++ target_name_to_solr_col_map
		val solr_col_to_name_map = name_to_solr_col_map.map(_.swap)
		val final_solr_cols = solr_col_to_name_map.keys.toArray
		final_anomaly_df = final_anomaly_df.select(final_solr_cols.head, final_solr_cols.tail: _*)

		val final_orig_cols = final_solr_cols.map(c => final_anomaly_df(c).as(solr_col_to_name_map(c)))
		var final_anomaly_df_orig_cols = final_anomaly_df.select(final_orig_cols: _*)
		final_anomaly_df_orig_cols = final_anomaly_df_orig_cols.select("DocumentName", "Anomaly", "Network_Name", "Target_Network_Name", "Provider_Federal_Tax_Identifier", "Anomaly_Type")

		final_anomaly_df_orig_cols = final_anomaly_df_orig_cols.distinct
		final_anomaly_df_orig_cols.cache()
		println("Final Anomaly DF Orig Cols Count " + final_anomaly_df_orig_cols.count())

		val grouped_abc = final_anomaly_df_orig_cols.groupBy("DocumentName", "Provider_Federal_Tax_Identifier", "Anomaly_Type").agg(concat_ws("|", sort_array(collect_set(col("Anomaly")))) as "Anomaly_Set", concat_ws("|", sort_array(collect_set(col("Network_Name")))) as "Network_Set", concat_ws("|", sort_array(collect_set(col("Target_Network_Name")))) as "Target_Network_Set")

		val mapped_abc = grouped_abc.withColumn("Map", map(col("Anomaly_Type"), col("Anomaly_Set")))
		val mergeExpr = expr("aggregate(Set_List, map(), (acc, i) -> map_concat(acc, i))")
		val mapped_grouped_abc = mapped_abc.groupBy("DocumentName", "Provider_Federal_Tax_Identifier", "Network_Set", "Target_Network_Set").agg(sort_array(collect_list(col("Anomaly_Type"))) as "Type_List", collect_list(col("Map")) as "Set_List").select(col("DocumentName"), col("Provider_Federal_Tax_Identifier"), col("Network_Set"), col("Target_Network_Set"), col("Type_List"), mergeExpr.as("Set_List")).select("DocumentName", "Provider_Federal_Tax_Identifier", "Network_Set", "Target_Network_Set", "Set_List", "Type_List")

		var drilled_down_df = mapped_grouped_abc.select(col("*"), explode(col("Set_List"))).groupBy("DocumentName", "Provider_Federal_Tax_Identifier", "Network_Set", "Target_Network_Set", "Type_List").pivot("key").agg(first(col("value")))

		drilled_down_df = drilled_down_df.withColumn("Feature_Type", lit(feature_extracted))
		drilled_down_df = drilled_down_df.withColumn("Usecase_ID", lit(usecase_id.toString))
		drilled_down_df = drilled_down_df.withColumn("Findings", concat_ws(", ", col("Type_List")))
		drilled_down_df = drilled_down_df.select("DocumentName", "Provider_Federal_Tax_Identifier", "Feature_Type", "Network_Set", "Target_Network_Set", add_col_name, missing_col_name, "Findings", "Usecase_ID")

		drilled_down_df = mkNull2Empty(drilled_down_df)
		drilled_down_df.write.mode("overwrite").save(usecase_path + "explore_by_source_drilled_down")

		var explore_by_source = drilled_down_df.withColumn("Num_Missing_Nets", when(col(missing_col_name).isNull, 0).otherwise(size(split(col(missing_col_name), "\\|")))).withColumn("Num_Additional_Nets", when(col(add_col_name).isNull, 0).otherwise(size(split(col(add_col_name), "\\|")))).select("DocumentName", "Feature_Type", "Findings", "Num_Missing_Nets", "Num_Additional_Nets")

		explore_by_source = explore_by_source.withColumnRenamed("Findings", "Type")

		val type_finding = udf((x: Int, y: Int) => {
			val missing_num = x.toString
			val add_num = y.toString
			val finding = s"$missing_num missing, $add_num additional"
			finding
		})

		explore_by_source = explore_by_source.withColumn("Findings", type_finding(col("Num_Missing_Nets"), col("Num_Additional_Nets"))).select("DocumentName", "Feature_Type", "Type", "Findings")

		explore_by_source = mkNull2Empty(explore_by_source)
		explore_by_source.write.mode("overwrite").save(usecase_path + "explore_by_source")

		var missing_df = final_anomaly_df_orig_cols.filter(col("Anomaly_Type") === missing_col_name).select("DocumentName", "Anomaly")
		missing_df = missing_df.groupBy("Anomaly").count().withColumnRenamed("count", "Num_Files")
		missing_df = missing_df.withColumn("Type", lit("Missing"))

		var additional_df = final_anomaly_df_orig_cols.filter(col("Anomaly_Type") === add_col_name).select("DocumentName", "Anomaly")
		additional_df = additional_df.groupBy("Anomaly").count().withColumnRenamed("count", "Num_Files")
		additional_df = additional_df.withColumn("Type", lit("Additional"))

		var explore_by_feature = missing_df.union(additional_df)
		explore_by_feature = explore_by_feature.withColumn("Feature_Type", lit(feature_extracted))

		val enter_finding = udf((x: Int, y: String) => {
			val num = x.toString
			val finding = s"$num $y anomalies"
			finding
		})

		explore_by_feature = explore_by_feature.withColumn("Findings", enter_finding(col("Num_Files"), col("Type"))).select("Anomaly", "Feature_Type", "Type", "Findings")

		explore_by_feature = mkNull2Empty(explore_by_feature)
		explore_by_feature.write.mode("overwrite").save(usecase_path + "explore_by_feature")

	}

}
