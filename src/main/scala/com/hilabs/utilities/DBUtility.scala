package com.hilabs.utilities

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, lit, trim, when}

import java.sql.Connection

object DBUtility {

	def genMapsFromValuesets(connection:Connection,
	                         valueset_names: Array[String]): (Map[String, String], Map[String, Int]) = {

		valueset_names.foldLeft((Map[String, String](), Map[String, Int]()))(
			(x, valueset_name) => {
				val name_to_solr_col_map = x._1
				val solr_col_to_datatype_map = x._2
				val statement = connection.createStatement()
				val query = "SELECT solrcolumnname, datatype FROM valuesets where valueset_name=\"" + valueset_name + "\"" + " order by ID desc limit 1"
				println(query)
				val resultSet = statement.executeQuery(query)
				if (resultSet.next()) {
					val solrcolumnname = resultSet.getString("solrcolumnname")
					val datatype = resultSet.getInt("datatype")
					(name_to_solr_col_map + (valueset_name -> solrcolumnname),
						solr_col_to_datatype_map + (solrcolumnname -> datatype))
				} else {
					x
				}
			})
	}

	def genMapsFromTempValuesets(connection: Connection,
	                             connection_id: Int,
	                             solrcolumnnames: Array[String]): (Map[String, String], Map[String, Int]) = {

		solrcolumnnames.foldLeft((Map[String, String](), Map[String, Int]()))(
			(x, solrcolumnname) => {
				val solr_col_to_name_map = x._1
				val solr_col_to_datatype_map = x._2
				val statement = connection.createStatement()
				val query = "SELECT valueset_name, datatype FROM temp_valuesets where solrcolumnname=\"" + solrcolumnname + "\"" + " and connection_id=" + connection_id
				println(query)
				val resultSet = statement.executeQuery(query)
				if (resultSet.next()) {
					val valueset_name = resultSet.getString("valueset_name")
					val datatype = resultSet.getInt("datatype")
					(solr_col_to_name_map + (solrcolumnname -> valueset_name),
						solr_col_to_datatype_map + (solrcolumnname -> datatype))
				} else {
					x
				}
			})
	}

	def getUseCaseName(connection: Connection,
	                   testModelId: Int): String = {

		val statement = connection.createStatement()
		var anomaly_conn_query = s"""select label_set_id from nlp_model_details where id in (select reference_model_id from nlp_model_details where id=$testModelId)"""
		val resultSet = statement.executeQuery(anomaly_conn_query)
		resultSet.next()
		val label_set_id = resultSet.getInt("label_set_id")
		anomaly_conn_query = s"""select name from labels_library where label_set_id=$label_set_id"""
		val resultSet1 = statement.executeQuery(anomaly_conn_query)
		resultSet1.next()
		val useCaseName = resultSet1.getString("name")
		useCaseName
	}

	def insertLabelsIntoConnectionList(connection: Connection,
	                                   basePath: String,
	                                   usecase_id: Int,
	                                   labels_type: String,
	                                   df_name: String,
	                                   solr_col_to_name_map: scala.collection.Map[String, String]): Int = {

		val connection_name = s"Explore by $labels_type for UseCase " + usecase_id
		val connection_query = "Select " + solr_col_to_name_map.keys.foldLeft("")((query, solr_col) => {
			query + solr_col_to_name_map(solr_col) + " AS " + "`" + solr_col + "`, "
		}).dropRight(2)
		val database_name = basePath + "/" + df_name
		val query = s"""insert into connectionlist (connectionname, connectiontype, database_name, file_system_type, connection_query, status_code) values("$connection_name", "Hadoop_Local_Copy", "$database_name", "PARQUET", "$connection_query", 41)"""
		val statement = connection.createStatement()
		statement.executeUpdate(query)
		val anomaly_conn_query = s"""select MAX(id) as max_id from connectionlist where connectionname="$connection_name""""
		val resultSet = statement.executeQuery(anomaly_conn_query)
		resultSet.next()
		resultSet.getInt("max_id")
	}

	def getProjectID(connection: Connection,
	                 connection_id: Int): Int = {

		val statement = connection.createStatement()
		val query = s"""select project_id from connection_project_details where connection_id="$connection_id""""
		val resultSet = statement.executeQuery(query)
		resultSet.next()
		resultSet.getInt("project_id")
	}

	def getModelName(connection: Connection,
	                 model_id: Int): String = {

		val statement = connection.createStatement()
		val anomaly_conn_query = s"""select name from nlp_model_details where id="$model_id""""
		val resultSet = statement.executeQuery(anomaly_conn_query)
		resultSet.next()
		resultSet.getString("name")
	}

	def getTargetConnectionId(connection: Connection,
	                          connection_id: Int,
	                          usecase_id: Int): Int = {

		val statement = connection.createStatement()
		val anomaly_conn_query = s"""select target_connection from labels_library where connection_id=$connection_id and label_set_id=$usecase_id"""
		val resultSet = statement.executeQuery(anomaly_conn_query)
		resultSet.next()
		resultSet.getInt("target_connection")
	}

	def insertIntoConnectionList(connection: Connection,
	                             database_name: String,
	                             model_id: Int,
	                             solr_col_to_name_map: scala.collection.Map[String, String]): Int = {

		val connection_name = "NLP Final Anomalies for Model " + model_id
		val connection_query = "Select " + solr_col_to_name_map.keys.foldLeft("")((query, solr_col) => {
			query + solr_col_to_name_map(solr_col) + " AS " + "`" + solr_col + "`, "
		}).dropRight(2)
		val query = s"""insert into connectionlist(connectionname, connectiontype, database_name, file_system_type, connection_query, status_code) values("$connection_name", "Hadoop_Local_Copy","$database_name", "PARQUET", "$connection_query", 41)"""
		val statement = connection.createStatement()
		statement.executeUpdate(query)
		val anomaly_conn_query = s"""select MAX(id) as max_id from connectionlist where connectionname="$connection_name""""
		val resultSet = statement.executeQuery(anomaly_conn_query)
		resultSet.next()
		resultSet.getInt("max_id")
	}

	def insertIntoTempValuesets(connection: Connection,
	                            final_anomaly_df: DataFrame,
	                            connection_id: Int,
	                            solr_col_to_name_map: scala.collection.Map[String, String],
	                            solr_col_to_datatype_map: scala.collection.Map[String, Int]): Unit = {

		final_anomaly_df.columns.foreach(c => {
			val valueset_name = solr_col_to_name_map(c)
			val datatype = solr_col_to_datatype_map(c)
			val query = s"""insert into temp_valuesets (ColumnUniqueName, valueset_name, connection_id, datatype, solrcolumnname, status, orig_valueset_name, mapping_type, file_column_name) values("$c", "$valueset_name", $connection_id, $datatype, "$c", 1, "$valueset_name", 4, "$c")"""
			println(query)
			val statement = connection.createStatement()
			statement.executeUpdate(query)
		})
	}

	def getSourceLabelsKeyTargetFeatures(connection: Connection,
	                                     connection_id: Int,
	                                     usecase_id: Int): (Array[String], Array[String], Array[String]) = {

		val statement = connection.createStatement()

		val anomaly_conn_query = s"""select features,key_features,target_feature from labels_library where connection_id=$connection_id and label_set_id=$usecase_id"""
		val resultSet1 = statement.executeQuery(anomaly_conn_query)
		resultSet1.next()
		val features = resultSet1.getString("features").split(',')
		val key_features = resultSet1.getString("key_features").split(',')
		val target_features = resultSet1.getString("target_feature").split(',')
		(features, key_features, target_features)

	}

	def genColumnNamesFromValuesets(connection: Connection,
	                                solrcolumnnames: Array[String]): Array[String] = {

		solrcolumnnames.map { solrcolumnname =>
			val statement = connection.createStatement()
			val query = "SELECT valueset_name FROM valuesets where solrcolumnname=\"" + solrcolumnname + "\""
			println(query)
			val resultSet = statement.executeQuery(query)
			if (resultSet.next()) {
				val valueset_name = resultSet.getString("valueset_name")
				valueset_name
			}
			else {
				"None in Result Set"
			}
		}
	}

	def updateLabelsLibraryDetails(connection: Connection,
	                               base_conn_id: Int,
	                               usecase_id: Int,
	                               labels_type: String,
	                               labels_connection_id: Int): Unit = {

		if (labels_type.equals("Feature")) {
			val query = s"""update labels_library set feature_connection_id=$labels_connection_id, status='Indexed Explore by Feature' where label_set_id=$usecase_id and connection_id=$base_conn_id"""
			println(s"Executing Query: $query")
			val statement = connection.createStatement()
			statement.executeUpdate(query)
		}

		else if (labels_type.equals("Source")){
			val query = s"""update labels_library set source_connection_id=$labels_connection_id, status='Indexed Explore by Source' where label_set_id=$usecase_id and connection_id=$base_conn_id"""
			println(s"Executing Query: $query")
			val statement = connection.createStatement()
			statement.executeUpdate(query)
		}

		else if (labels_type.equals("Drill")){
			val query = s"""update labels_library set source_drill_connection_id=$labels_connection_id, status='Indexed Explore by Source Drilled Down' where label_set_id=$usecase_id and connection_id=$base_conn_id"""
			println(s"Executing Query: $query")
			val statement = connection.createStatement()
			statement.executeUpdate(query)
		}
	}

	def updateNLPModelDetails(connection: Connection,
	                          model_id: Int,
	                          anomaly_connection_id: Int): Int = {

		val query = s"""update nlp_model_details set output_connection_id=$anomaly_connection_id, status="SUCCEEDED"where id=$model_id"""
		println(query)
		val statement = connection.createStatement()
		statement.executeUpdate(query)
	}

	def mkNULL(src:DataFrame):DataFrame={
		var src_null = src
		for(src_col <- src_null.columns){
			src_null = src_null.withColumn(src_col,trim(col(src_col)))
			src_null= src_null.withColumn(src_col, when(col(src_col) === "", lit(null)).otherwise(col(src_col)))
		}
		src_null
	}

}
