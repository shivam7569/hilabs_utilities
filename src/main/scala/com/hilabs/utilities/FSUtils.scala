package com.hilabs.utilities

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.fs.permission.FsPermission
import org.apache.spark.SparkContext
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, lit, trim, when}

object FSUtils {

	def checkIfPathExistsInHadoop(path: String, sc: SparkContext): Boolean = {

		val conf = sc.hadoopConfiguration
		val fs = FileSystem.get(conf)
		fs.exists(new Path(path))
	}

	def makeDirectoryInHDFS(SEED_PATH: String, filepath: String, newPermission: String, sc: SparkContext): Unit = {

		val hadoopConf = sc.hadoopConfiguration
		val hdfs = FileSystem.get(hadoopConf)
		val pathString = if (!filepath.startsWith(SEED_PATH)) SEED_PATH + filepath else filepath
		val setPermission = if (newPermission == "") "755" else newPermission
		hdfs.mkdirs(new Path(pathString), new FsPermission(setPermission))
		System.out.println("new directory in hdfs created with permissions " + newPermission)
	}

	def mkNull2Empty(src: DataFrame): DataFrame ={
		var src_null = src
		for(src_col <- src_null.columns){
			src_null = src_null.na.fill("", Array(src_col))
		}
		src_null
	}
}
