package com.hilabs.v2_epds

import org.apache.spark.SparkContext
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SQLContext, SparkSession}

class CFF_Utilities {

  def process_CFF(loadrunid: String): Unit ={

    val spark = SparkSession.builder.master("yarn").
      appName("POLAR_EXTRACTION").
      getOrCreate
    val sc: SparkContext = spark.sparkContext
    val sqlContext: SQLContext  = spark.sqlContext

    spark.conf.set("spark.sql.crossJoin.enabled", value = true)

    println(s"Spark-Context: $sc")
    println(s"SQL-Context: $sqlContext")

    val dd = loadrunid.slice(6, 8)
    val mm = loadrunid.slice(4, 6)
    val yyyy = loadrunid.slice(0, 4)
    val date = dd + "-" + mm + "-" + yyyy

    val tables: Array[String] = Array("pnet", "prmb", "apspt", "apadr")

    println(f"Date: $date")
    println(f"LoadRunID: $loadrunid")
    println(f"Preprocessing Tables: ${tables.mkString("Array(", ", ", ")")}")

    val fileHeadMap = spark.read.format("csv").option("header", value = true).load("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/shivam/CFF_HEADERS.csv")

    def readSkipData(path: String, HeaderMap: DataFrame, FileName: String): DataFrame={
      val fi = sc.textFile(path)
      val da = fi.mapPartitionsWithIndex{ (id_x, iter) => if (id_x == 0) iter.drop(1) else iter }
      val rowRdd = da.map(x => Row(x))
      val x = Array("str")
      val myschema = StructType(x.map(fieldName ⇒ StructField(fieldName, StringType, nullable = true)))
      var temp_df = spark.createDataFrame(rowRdd, myschema)
      val split_col = split(temp_df("str"), "~")
      val headStag = HeaderMap.filter(HeaderMap("TABLE_NAME")===FileName)
      for (value <- headStag.select("COLUMN_NAMES").collect){
        for ((x, i) <- value.toString.replace("[", "").replace("]","").split(",").zipWithIndex){
          temp_df = temp_df.withColumn(x, split_col.getItem(i))
        }
      }
      temp_df = temp_df.drop(col("str"))
      temp_df
    }

    def mkNULL(src: DataFrame): DataFrame={
      var src_null = src
      for(src_col <- src_null.columns){
        src_null = src_null.withColumn(src_col,trim(col(src_col)))
        src_null= src_null.withColumn(src_col, when(col(src_col) === "", lit(null)).otherwise(col(src_col)))
      }
      src_null
    }

    for (x <- tables){
      val file_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/shivam/Data_Profiling/CFF_FILES/" + x.toUpperCase()
      val dirpath = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CFF_DATA/cff/" + loadrunid + "/"
      val table_path = dirpath + x.toUpperCase() + "." + loadrunid + ".dat"

      var table = readSkipData(table_path, fileHeadMap, x.toUpperCase())

      table = mkNULL(table)

      println(f"Writing CFF ${x.toUpperCase()}...")
      table.write.mode("overwrite").parquet(file_path)
      println(f" CFF ${x.toUpperCase()} written!")
    }

  }

}
