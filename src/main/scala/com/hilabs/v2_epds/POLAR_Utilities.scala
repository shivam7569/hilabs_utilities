package com.hilabs.v2_epds

import org.apache.spark.SparkContext
//import org.apache.spark.sql.functions.{col, lit, trim, when}
import org.apache.spark.sql.{DataFrame, SQLContext, SparkSession}

class POLAR_Utilities {

	def connect_to_polar(table_name: String,
	               num_partitions: Int = 100,
	               connection_url: String = "jdbc:db2://va10p10171a:30215/POLARDB",
	               connection_user: String = "srcmchk",
	               connection_password: String = "s6J#@+vTFm",
	               connection_driver: String = "com.ibm.db2.jcc.DB2Driver"): DataFrame = {

		val table: String = table_name
		val url: String = connection_url
		val user: String = connection_user
		val password: String = connection_password
		val driver: String = connection_driver
		val True: Boolean = true

		val spark: SparkSession = SparkSession.builder().getOrCreate()
		val query_max = s"""(SELECT EP_ID FROM $table_name ORDER BY EP_ID DESC LIMIT 1) foo"""
		val query_min = s"""(SELECT EP_ID FROM $table_name ORDER BY EP_ID LIMIT 1) foo"""

		val polar_max: Long = spark.read.format("jdbc")
				.option("mode", "overwrite")
				.option("truncate", True)
				.option("url", url)
				.option("ssl", True)
				.option("sslmode", "require")
				.option("sslConnection", True)
				.option("driver", driver)
				.option("dbtable", query_max)
				.option("user", user)
				.option("password", password)
				.load().collect()(0)(0).asInstanceOf[Long]

		val polar_min: Long = spark.read.format("jdbc")
				.option("mode", "overwrite")
				.option("truncate", True)
				.option("url", url)
				.option("ssl", True)
				.option("sslmode", "require")
				.option("sslConnection", True)
				.option("driver", driver)
				.option("dbtable", query_min)
				.option("user", user)
				.option("password", password)
				.load().collect()(0)(0).asInstanceOf[Long]

		val polar: DataFrame = spark.read.format("jdbc")
				.option("mode", "overwrite")
				.option("partitionColumn", "ep_id")
				.option("lowerBound", polar_min)
				.option("upperBound", polar_max)
				.option("numPartitions", num_partitions)
				.option("truncate", True)
				.option("url", url)
				.option("ssl", True)
				.option("sslmode", "require")
				.option("sslConnection", True)
				.option("driver", driver)
				.option("dbtable", table)
				.option("user", user)
				.option("password", password)
				.load()

		polar
	}

	def get_connection_id(table_name: String,
												connection_url: String = "jdbc:mysql://dwbdtest1e1v.wellpoint.com/syndata2008_2010",
												connection_user: String = "root",
												connection_password: String = "Hilabs@123",
												connection_driver: String = "com.mysql.jdbc.Driver"): Int ={

		val table: String = table_name.toUpperCase()
		val url: String = connection_url
		val user: String = connection_user
		val password: String = connection_password
		val driver: String = connection_driver
		val False: Boolean = false
		val True: Boolean = true

		val spark: SparkSession = SparkSession.builder().getOrCreate()
		val query: String = s"""(SELECT connection_id FROM polar_db_details WHERE table_name = '$table') foo"""
		val pnet_conn_id = spark.read.format("jdbc").option("mode", "overwrite").option("truncate", True).option("url", url).option("useSSL", False).option("sslConnection", False).option("driver", driver).option("dbtable", query).option("user", user).option("password", password).load().collect()(0)(0).asInstanceOf[Int]

		pnet_conn_id
	}

	def get_latest_loadID(): Unit = {

		val spark = SparkSession.builder.master("yarn").
			appName("POLAR_GET_LOADID").
			getOrCreate
		val sc: SparkContext = spark.sparkContext
		val sqlContext: SQLContext  = spark.sqlContext
		import sqlContext.implicits._

		spark.conf.set("spark.sql.crossJoin.enabled", value = true)

		println(s"Spark-Context: $sc")
		println(s"SQL-Context: $sqlContext")

		val query_pnet: String = """(select src_file_nm from cff.load_cntrl_dtl where src_file_nm like 'PNET%' order by load_run_id desc limit 1) foo"""
		val query_prmb: String = """(select src_file_nm from cff.load_cntrl_dtl where src_file_nm like 'PRMB%' order by load_run_id desc limit 1) foo"""
		val query_apspt: String = """(select src_file_nm from cff.load_cntrl_dtl where src_file_nm like 'APSPT%' order by load_run_id desc limit 1) foo"""
		val query_apadr: String = """(select src_file_nm from cff.load_cntrl_dtl where src_file_nm like 'APADR%' order by load_run_id desc limit 1) foo"""

		println("[INFO] Extracting last ingested load in POLAR PNET...")
		val pnet_load = spark.read.format("jdbc").option("mode", "overwrite").option("truncate", value = true).option("url", "jdbc:db2://va10p10171a:30215/POLARDB").option("ssl", "true").option("sslmode", "require").option("sslConnection","true").option("driver", "com.ibm.db2.jcc.DB2Driver").option("dbtable", query_pnet).option("user", "srcmchk").option("password", "s6J#@+vTFm").load().collect()(0)(0).asInstanceOf[String].split('.')(1)
		println("[INFO] Extracting last ingested load in POLAR PRMB...")
		val prmb_load = spark.read.format("jdbc").option("mode", "overwrite").option("truncate", value = true).option("url", "jdbc:db2://va10p10171a:30215/POLARDB").option("ssl", "true").option("sslmode", "require").option("sslConnection","true").option("driver", "com.ibm.db2.jcc.DB2Driver").option("dbtable", query_prmb).option("user", "srcmchk").option("password", "s6J#@+vTFm").load().collect()(0)(0).asInstanceOf[String].split('.')(1)
		println("[INFO] Extracting last ingested load in POLAR APSPT...")
		val apspt_load = spark.read.format("jdbc").option("mode", "overwrite").option("truncate", value = true).option("url", "jdbc:db2://va10p10171a:30215/POLARDB").option("ssl", "true").option("sslmode", "require").option("sslConnection","true").option("driver", "com.ibm.db2.jcc.DB2Driver").option("dbtable", query_apspt).option("user", "srcmchk").option("password", "s6J#@+vTFm").load().collect()(0)(0).asInstanceOf[String].split('.')(1)
		println("[INFO] Extracting last ingested load in POLAR APADR...")
		val apadr_load = spark.read.format("jdbc").option("mode", "overwrite").option("truncate", value = true).option("url", "jdbc:db2://va10p10171a:30215/POLARDB").option("ssl", "true").option("sslmode", "require").option("sslConnection","true").option("driver", "com.ibm.db2.jcc.DB2Driver").option("dbtable", query_apadr).option("user", "srcmchk").option("password", "s6J#@+vTFm").load().collect()(0)(0).asInstanceOf[String].split('.')(1)

		val data = Seq((pnet_load, prmb_load, apspt_load, apadr_load))

		val loads = data.toDF()

		val df = loads.withColumnRenamed("_1", "PNET_LOAD").withColumnRenamed("_2", "PRMB_LOAD").withColumnRenamed("_3", "APSPT_LOAD").withColumnRenamed("_4", "APADR_LOAD")

		df.write.mode("overwrite").json("/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/shivam/Data_Profiling/DOCS/polar_loads")

	}

	def extract_POLAR(tables: String,
										write: Boolean = true): Unit ={
		val spark = SparkSession.builder.master("yarn").
			appName("POLAR_EXTRACTION").
			getOrCreate
		val sc: SparkContext = spark.sparkContext
		val sqlContext: SQLContext  = spark.sqlContext

		println(s"Spark-Context: $sc")
		println(s"SQL-Context: $sqlContext")

		val tables_to_extract = tables.split(',')
		println(tables_to_extract.mkString("Array(", ", ", ")"))

//		def mkNULL(src:DataFrame):DataFrame={
//			var src_null = src
//			for(src_col <- src_null.columns){
//				src_null = src_null.withColumn(src_col,trim(col(src_col)))
//				src_null= src_null.withColumn(src_col, when(col(src_col) === "", lit(null)).otherwise(col(src_col)))
//			}
//			src_null
//		}

		val obj = new POLAR_Utilities()
		for (x <- tables_to_extract){
			println(f"Extracting POLAR ${x.toUpperCase()}")
			val table_save_path = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/shivam/Data_Profiling/POLAR_FILES/" + x.toUpperCase()
			val table_connection = obj.connect_to_polar("cff.cff_" + x)
//			table_connection = mkNULL(table_connection)

			if (write) {
				println(f"Writing POLAR ${x.toUpperCase()}...")
				table_connection.write.mode("overwrite").parquet(table_save_path)
				println(f"POLAR ${x.toUpperCase()} written!")
			}

		}

	}

}
