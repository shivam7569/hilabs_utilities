package com.hilabs;

import com.hilabs.nlp.NLP_Views;
import com.hilabs.nlp.Network_Allocation;
import com.hilabs.v2_epds.CFF_Utilities;
import com.hilabs.nlp.NLP_Utilities;
import com.hilabs.v2_epds.POLAR_Utilities;
import org.json.JSONObject;

public class MainClass {
    public static void main(String[] args){

        String jsonInString = args[0];
        jsonInString = CharacterEncoderHandler.characterEncoder(jsonInString);
        JSONObject json = new JSONObject(jsonInString);
        String functionality = json.getString("Functionality");
        System.out.printf("Functionality: %s%n", functionality);

        String SEED_PATH = "/ts/hdfsdata/ve2/dqc/mchk/phi/no_gbd/r000/inbound/CDA/";

        if (functionality.equals("Get_POLAR_Loads")){
            POLAR_Utilities obj = new POLAR_Utilities();
            obj.get_latest_loadID();
        }

        if (functionality.equals("Extract_POLAR")){
            String parameters = json.getString("Parameters");
            boolean write = Boolean.parseBoolean(json.getString("Write"));
            System.out.printf("Parameters: %s%n", parameters);
            POLAR_Utilities obj = new POLAR_Utilities();
            obj.extract_POLAR(parameters, write);
        }

        if (functionality.equals("Preprocess_CFF")){
            String parameters = json.getString("Parameters");
            System.out.printf("Parameters: %s%n", parameters);
            CFF_Utilities obj = new CFF_Utilities();
            obj.process_CFF(parameters);
        }

        if (functionality.equals("Index_NLP_Explore_by_Source")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);
            System.out.printf("SEED_PATH: %s%n", SEED_PATH);

            NLP_Utilities obj = new NLP_Utilities();
            obj.index_explore_by_source(connection_id, usecase_id, SEED_PATH);
        }

        if (functionality.equals("Index_NLP_Explore_by_Feature")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);
            System.out.printf("SEED_PATH: %s%n", SEED_PATH);

            NLP_Utilities obj = new NLP_Utilities();
            obj.index_explore_by_feature(connection_id, usecase_id, SEED_PATH);
        }

        if (functionality.equals("Index_NLP_Explore_by_Source_Drilled_Down")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);
            System.out.printf("SEED_PATH: %s%n", SEED_PATH);

            NLP_Utilities obj = new NLP_Utilities();
            obj.index_explore_by_source_drilled_down(connection_id, usecase_id, SEED_PATH);
        }

        if (functionality.equals("Index_NLP_All_Views")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);
            System.out.printf("SEED_PATH: %s%n", SEED_PATH);

            NLP_Utilities obj = new NLP_Utilities();
            obj.index_explore_by_source(connection_id, usecase_id, SEED_PATH);
            obj.index_explore_by_feature(connection_id, usecase_id, SEED_PATH);
            obj.index_explore_by_source_drilled_down(connection_id, usecase_id, SEED_PATH);
        }

        if (functionality.equals("Generate_NLP_Views")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);
            System.out.printf("SEED_PATH: %s%n", SEED_PATH);

            NLP_Views.generate_views(connection_id, usecase_id, SEED_PATH);
        }

        if (functionality.equals("Allocate_Networks")){
            int connection_id = Integer.parseInt(json.getString("Connection_ID"));
            int usecase_id = Integer.parseInt(json.getString("Usecase_ID"));

            System.out.printf("Connection_ID: %s%n", connection_id);
            System.out.printf("Usecase_ID: %s%n", usecase_id);

            Network_Allocation.allocateNetworks(connection_id, usecase_id);
        }
    }
}
