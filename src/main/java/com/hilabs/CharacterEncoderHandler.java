package com.hilabs;

@SuppressWarnings("unused")
public class CharacterEncoderHandler {

    public static String characterEncoder(String stringToEncode){

        stringToEncode = stringToEncode.replaceAll("~", "\"");
        stringToEncode = stringToEncode.replaceAll("%20", " ");
        stringToEncode = stringToEncode.replaceAll("%27", "'");
        stringToEncode = stringToEncode.replaceAll("\n", " ");

        return stringToEncode;
    }

}
